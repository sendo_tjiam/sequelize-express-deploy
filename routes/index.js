const router = require('express').Router();
const usersRoute = require('./users.routes');
const postsRoute = require('./posts.routes');
const loginRoute = require('./login.routes');
const categoriesRoute = require('./categories.routes');
const postsCategoriesRoute = require('./postsCategories.routes');

router.use('/auth', loginRoute);
router.use('/users', usersRoute);
router.use('/posts/categories', postsCategoriesRoute);
router.use('/posts', postsRoute);
router.use('/categories', categoriesRoute);

module.exports = router;
