const { LoginController } = require('../controllers');
const router = require('express').Router();

router.post('/login', LoginController.login);

router.post('/logout', LoginController.logout)

module.exports = router;
