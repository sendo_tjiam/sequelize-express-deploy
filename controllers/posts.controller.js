const { Users, Posts } = require('../models');
const uuid = require('uuid');

class PostsController {
	static getPosts = async (req, res) => {
		try {
			const options = {
				include: [
					{
						model: Users,
					},
				],
			};
			const post = await Posts.findAll(options);
			res.status(200).json(post);
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};
	static getPostById = async (req, res) => {
		try {
			const { id } = req.params;
			const options = {
				where: {
					id,
				},
			};
			const post = await Posts.findOne(options);
			if (!post) {
				res.status(200).json({
					message: `Find no post with id ${id}`,
				});
			}
			res.status(200).json({ post });
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};
	static createPost = async (req, res) => {
		try {
			const { title, description, UserId } = req.body;
			if (!title || !description || !UserId) {
				res.status(200).json({
					status: 'Failed',
					message: 'Function required body field',
				});
				return;
			}
			const id = uuid.v4();
			const payload = {
				id,
				title,
				description,
				UserId,
			};

			const newPost = await Posts.create(payload);
			res.status(200).json({ data: newPost });
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};
	static updatePost = async (req, res) => {
		try {
			const { id } = req.params;
			const options = {
				where: {
					id,
				},
				returning: true,
			};
			const { title, description } = req.body;
			const payload = {
				title,
				description,
			};
			const updated = await Posts.update(payload, options);
			if (!updated) {
				res.status(200).json({
					message: `Find no post with id ${id}`,
				});
				return;
			}
			res.status(200).json({
				data: updated,
			});
			res.status(400).json({ error });
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};
	static deletePost = async (req, res) => {
		try {
			const { id } = req.params;
			const deleted = await Posts.destroy({
				where: {
					id,
				},
				returning: true,
			});
			if (!deleted) {
				res.status(200).json({
					message: `Find no post with id ${id}`,
				});
				return;
			}
			res.status(200).json({
				data: deleted,
			});
			res.status(400).json({ error });
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};
}

module.exports = PostsController;
