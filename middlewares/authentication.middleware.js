const { Token } = require('../utils');
function authentication(req, res, next) {
	const { session_token } = req.headers;
	const payload = session_token ? Token.decodeToken(session_token) : 0;
	console.log(payload, '<<<<');
	if (!payload || !session_token) {
		res.status(401).json({
			status: 401,
			msg: 'Please login',
		});
	} else {
		req.user_login = payload;
		next();
	}
}

module.exports = authentication;
