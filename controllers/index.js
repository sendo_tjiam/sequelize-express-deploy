const PostsController = require('./posts.controller');
const UsersController = require('./users.controller');
const LoginController = require('./login.controller');
const CategoriesController = require('./categories.controller');
const PostsCategoriesController = require('./postsCategories.controller');

module.exports = {
	PostsController,
	UsersController,
	LoginController,
	CategoriesController,
	PostsCategoriesController,
};
