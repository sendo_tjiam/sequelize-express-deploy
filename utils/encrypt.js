const bcrypt = require('bcryptjs');

class Encryption {
	static encryptPassword(rawPassword) {
		try {
			const salt = bcrypt.genSaltSync(10);
			const hash = bcrypt.hashSync(rawPassword, salt);
			return hash;
		} catch (error) {
			return null;
		}
	}

	static isValidPassword(rawPassword, hash) {
		try {
			return bcrypt.compareSync(rawPassword, hash);
		} catch (error) {
			return null;
		}
	}
}

module.exports = Encryption;
