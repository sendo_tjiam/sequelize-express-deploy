const { UsersController } = require('../controllers');
const router = require('express').Router();
const { authentication, authorization } = require('../middlewares') 

router.get('/', UsersController.getUsers);
router.get('/:id', UsersController.getUserWithId);
router.post('/', UsersController.createUser);

router.put('/:id', [authentication, authorization], UsersController.updateUser);
router.delete('/:id', [authentication, authorization], UsersController.deleteUser);

module.exports = router;
