const { CategoriesController } = require('../controllers');
const router = require('express').Router();

router.get('/', CategoriesController.getCategories);
router.get('/:id', CategoriesController.getCategoryById);
router.post('/', CategoriesController.createCategory);

router.put('/:id', CategoriesController.updateCategory);
router.delete('/:id', CategoriesController.deleteCategory);

module.exports = router;
