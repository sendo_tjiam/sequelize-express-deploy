const { Users, Posts } = require('../models');
const uuid = require('uuid');
const { Encrypt } = require('../utils');
const e = require('express');

class UsersController {
	static getUsers = async (req, res) => {
		try {
			const options = {
				include: [
					{
						model: Posts,
					},
				],
			};
			const data = await Users.findAll(options);
			res.status(200).json(data);
		} catch (error) {
			console.log(error);
		}
	};

	static createUser = async (req, res) => {
		try {
			let { name, age, job, password } = req.body;
			if (!name || !age || !job || !password) {
				res.status(200).json({
					status: 'Failed',
					message: 'Function required body field',
				});
				return;
			}
			const id = uuid.v4();
			const payload = {
				id,
				name,
				age,
				job,
				password: Encrypt.encryptPassword(password),
			};
			console.log(payload);
			const newUser = await Users.create(payload);
			res.status(200).json({ data: newUser });
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};

	static updateUser = async (req, res) => {
		try {
			const { name, age, job } = req.body;

			const payload = {
				name,
				age,
				job,
			};
			const { id } = req.params;
			const options = {
				where: {
					id,
				},
				returning: true,
			};
			const updated = await Users.update(payload, options);
			if (!updated) {
				res.status(200).json({
					message: `Find no user with id ${id}`,
				});
				return;
			}
			res.status(200).json({
				data: updated,
			});
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};

	static deleteUser = async (req, res) => {
		try {
			const { id } = req.params;
			const deleted = await Users.destroy({
				where: {
					id,
				},
				returning: true,
			});
			if (!deleted) {
				res.status(200).json({
					message: `Find no user with id ${id}`,
				});
				return;
			}
			res.status(200).json({
				data: deleted,
			});
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};

	static getUserWithId = async (req, res) => {
		try {
			const { id } = req.params;
			const user = await Users.findOne({
				where: {
					id: id,
				},
			});
			if (!user) {
				res.status(200).json({
					message: `Find no user with id ${id}`,
				});
				return;
			}
			res.status(200).json({ data: user });
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};
}

module.exports = UsersController;
