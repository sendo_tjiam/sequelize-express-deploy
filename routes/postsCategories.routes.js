const { PostsCategoriesController } = require('../controllers');
const router = require('express').Router();

router.get('/', PostsCategoriesController.getPostsCategories);
router.get('/:id', PostsCategoriesController.getPostsCategoriesById);
router.post('/', PostsCategoriesController.createPostsCategories);

router.delete('/:id', PostsCategoriesController.deletePostsCategories);

module.exports = router;
