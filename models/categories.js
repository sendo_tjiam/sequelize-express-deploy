'use strict';
const { Model } = require('sequelize');
module.exports = (sequelize, DataTypes) => {
	class Categories extends Model {
		/**
		 * Helper method for defining associations.
		 * This method is not a part of Sequelize lifecycle.
		 * The `models/index` file will call this method automatically.
		 */
		static associate(models) {
			// define association here
			Categories.belongsToMany(models.Posts, {
				through: models.PostsCategories, as: 'Posts'
			});
		}
	}
	Categories.init(
		{
			id: {
				primaryKey: true,
				type: DataTypes.STRING,
			},
			category: DataTypes.STRING,
			tag: DataTypes.INTEGER,
		},
		{
			sequelize,
			modelName: 'Categories',
		},
	);
	return Categories;
};
