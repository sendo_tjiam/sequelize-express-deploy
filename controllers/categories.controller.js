const { Posts, Categories } = require('../models');
const uuid = require('uuid');

class CategoriesController {
	static getCategories = async (req, res) => {
		try {
			const options = {
				include: [
					{
						model: Posts,
						as: 'Posts',
					},
				],
			};
			const categories = await Categories.findAll(options);
			res.status(200).json({ categories });
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};

	static getCategoryById = async (req, res) => {
		try {
			const { id } = req.params;
			const options = {
				where: {
					id,
				},
			};
			const category = await Categories.findOne(options);
			if (!category) {
				res.status(200).json({
					message: `Find no category with id ${id}`,
				});
			}
			res.status(200).json({ category });
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};

	static createCategory = async (req, res) => {
		try {
			const { category, tag } = req.body;
			if (!category || !tag) {
				res.status(200).json({
					status: 'Failed',
					message: 'Function required body field',
				});
				return;
			}
			const id = uuid.v4();
			const payload = {
				id,
				category,
				tag,
			};
			const newCategory = await Categories.create(payload);
			console.log('test');
			res.status(200).json({ data: newCategory });
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};

	static updateCategory = async (req, res) => {
		try {
			const { category, tag } = req.body;
			const payload = {
				category,
				tag,
			};
			const { id } = req.params;
			const options = {
				where: {
					id,
				},
				returning: true,
			};
			const updated = await Posts.update(payload, options);
			if (!updated) {
				res.status(200).json({
					message: `Find no category with id ${id}`,
				});
				return;
			}
			res.status(200).json({
				data: updated,
			});
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};

	static deleteCategory = async (req, res) => {
		try {
			const { id } = req.params;
			const deleted = await Categories.destroy({
				where: {
					id,
				},
				returning: true,
			});
			if (!deleted) {
				res.status(200).json({
					message: `Find no category with id ${id}`,
				});
				return;
			}
			res.status(200).json({
				data: deleted,
			});
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};
}

module.exports = CategoriesController;
