const { Users } = require('../models');
const { Token, Encrypt } = require('../utils');

class LoginController {
	static login = async (req, res) => {
		try {
			const { name, password } = req.body;
			const user = await Users.findOne({
				where: {
					name,
				},
			});
			console.log(password, user.dataValues.password);
			const compare = user
				? Encrypt.isValidPassword(password, user.dataValues.password)
				: 0;
			if (!user || !compare) {
				res.status(401).json({
					status: 401,
					message: 'Unauthorize',
				});
				return;
			}
			const access_token = Token.generateToken(user.dataValues);
			res.status(200).json({ access_token });
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};

	static logout = async (req, res) => {
		try {
			res.status(200).json({
				status: 200,
				message: 'Success logout',
			});
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};
}

module.exports = LoginController;
