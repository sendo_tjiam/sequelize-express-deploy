const { Categories, Posts, PostsCategories } = require('../models');

class PostsCategoriesController {
	static getPostsCategories = async (req, res) => {
		try {
			const postsCategories = await PostsCategories.findAll();
			res.status(200).json({ postsCategories });
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};

	static getPostsCategoriesById = async (req, res) => {
		try {
			const { id } = req.params;
			const options = {
				where: {
					id,
				},
			};
			const postsCategories = await PostsCategories.findOne(options);
			if (!postsCategories) {
				res.status(200).json({
					message: `Find no post category with id ${id}`,
				});
			}
			res.status(200).json({ postsCategories });
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};

	static createPostsCategories = async (req, res) => {
		try {
			const { PostId, CategoryId } = req.body;
			if (!PostId || !CategoryId) {
				res.status(200).json({
					status: 'Failed',
					message: 'Function required body field',
				});
				return;
			}
			const payload = {
				PostId,
				CategoryId,
			};
			const postsCategories = await PostsCategories.create(payload);
			res.status(200).json({ data: postsCategories });
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};

	static deletePostsCategories = async (req, res) => {
		try {
			const { id } = req.params;
			const deleted = await PostsCategories.destroy({
				where: {
					id,
				},
				returning: true,
			});
			if (!deleted) {
				res.status(200).json({
					message: `Find no post category with id ${id}`,
				});
				return;
			}
			res.status(200).json({
				data: deleted,
			});
		} catch (error) {
			console.log(error);
			res.status(400).json({ error });
		}
	};
}

module.exports = PostsCategoriesController;
